# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'c4c/version'

Gem::Specification.new do |spec|
  spec.name          = "c4c"
  spec.version       = C4c::VERSION
  spec.authors       = ["Amar Daxini"]
  spec.email         = ["amardaxini@gmail.com"]
  spec.description   = %q{C4C}
  spec.summary       = %q{C4C basic of getting tweets}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
