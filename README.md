# C4c

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 'c4c'
    gem 'twitter' ,:git => 'https://github.com/sferik/twitter.git'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install c4c

## Usage
		require 'c4c'
		require 'bundler/setup'
		C4C.configure do |config|
		  config.consumer_key        = "CONSUMER KEY"
		  config.consumer_secret     = "CONSUMER SECRET"
		  config.access_token        = "Access Token"
		  config.access_token_secret = "Access Token Secret"
		end
		topics  = ["ruby"]
		tweet_analyzer = C4C::TweetAnalyzer.new(topics)
		tweet_analyzer.get_tweets
		tweets = tweet_analyzer.add_scores
		tweets = tweet_analyzer.sort_by_score
		tweets = tweet_analyzer.sort_by_time
		tweets = tweet_analyzer.sort_by_score_time
		tweets = tweet_analyzer.get_conversation_tweets
		tweets = tweet_analyzer.get_recent_conversation_tweets
		tweet = tweets[0]
		tweet.get_conversaation
		tweet.conversation_tweets

		# Tweet is twitter api object
		conversation = C4C::Conversation.new(tweet)
		conversation = C4C::Conversation.get_conversation_by_id(tweet_id)
		conversation.tweets #results in bottom up tweets last tweet is parent tweet
		

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
