require 'twitter'
require_relative 'configuration'
require_relative 'tweet'
module C4C
  class TweetAnalyzer
    attr_accessor :consumer_key,:consumer_secret,:access_token,:access_token_secret
    attr_accessor :keywords,:twitter_client,:no_of_tweets,:tweets,:tweet_response

    def initialize(keywords,attributes = {})
      @keywords= keywords
      @consumer_key = attributes[:consumer_key] || C4C.configuration.consumer_key
      @consumer_secret = attributes[:consumer_secret] || C4C.configuration.consumer_secret
      @access_token = attributes[:access_token] || C4C.configuration.access_token
      @access_token_secret = attributes[:access_token_secret] || C4C.configuration.access_token_secret
      @no_of_tweets = attributes[:no_of_tweets] || 100
      @since_id = attributes[:since_id]
      @max_id = attributes[:max_id]
      # Result Type Can be recent,mixed,popular
      @result_type = attributes[:result_type] || 'recent'
      @tweets = []
      @tweet_response = []
      get_client
    end

    # For Pagination
    def get_tweets
      if @no_of_tweets > 100

        search(@keywords.join(","),{:count => 100,:since_id=>@since_id,:max_id=>@max_id, :result_type => @result_type,:lang=>"en"})

        (1...@no_of_tweets/100).to_a.each do |x|
          begin
            if @tweet_response.last.next_results
              search(@keywords.join(","),@tweet_response.last.next_results)
            else
              break
            end
          rescue
          end
        end

      else
        search(@keywords.join(","), :count => @no_of_tweets,:since_id=>@since_id,:max_id=>@max_id, :result_type => @result_type,:lang=>"en")
      end
      process_tweets
      remove_duplicate_tweets
    end

    # Get All Conversation Tweet
    def get_conversation_tweets
      @tweets.select{|tw| tw.tweet.in_reply_to_status_id?}
    end

    def get_recent_conversation_tweets
      @tweets.select{|tw| tw.tweet.in_reply_to_status_id?}.sort_by {|x|x.tweet.created_at}
    end

    # Add Score
    def add_scores
      add_conversation_score
    end

    # Sort tweets based on score
    def sort_by_score
      @tweets.sort{|x,y|y.get_score.to_i<=>x.get_score.to_i}
    end
    def sort_by_score_time
      sort_by_score.sort_by{|x|x.tweet.created_at}
    end

    # Sort By Time
    def sort_by_time
      @tweets.sort_by {|x|x.tweet.created_at}
    end

    private
    # Add Conversation Score
    def add_conversation_score
      @tweets.select{|tw| tw.add_conversation_score }
    end

    def search(query,params)
      @tweet_response << @twitter_client.search(query,params)
    end

    def get_client

      @twitter_client = Twitter::REST::Client.new do |config|
        config.consumer_key        = @consumer_key
        config.consumer_secret     = @consumer_secret
        config.access_token        = @access_token
        config.access_token_secret = @access_token_secret
      end
    end

    def process_tweets
      @tweets ||=[]
      @tweet_response.each do  |tweet_response|
        tweet_response.collect do |tweet|
          @tweets << Tweet.new(tweet,Array(@keywords))
        end
      end
    end
    # Some Times Tweet has same text and tweet is shared by others bit they are pointing to same tweet
    # e,g
    # https://twitter.com/AScoubeau/status/387982856649441280
    # https://twitter.com/sum3rman/status/387930788127588352
    # will point to https://twitter.com/nivertech/status/387887966796152832

    def remove_duplicate_tweets
      @tweets =  tweets.uniq {|p| p.tweet.text}
    end
  end
end
