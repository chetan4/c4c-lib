module C4C
  class Tweet
    # tweet attribute is  search result tweet
    CONVERSATION_SCORE = 1
    FOLLOWER_SCORE = 0.002
    RETWEET_SCORE = 1.0
    FAVORITE_SCORE = 5.0
    HASHTAG_SCORE = 7.0

    attr_accessor :tweet,:score,:link_score,:conversation_score,:follower_score,:search_keywords
    attr_accessor :retweet_score,:favorite_score,:percentile_score,:total_score,:hashtag_score
    attr_accessor :conversation,:conversation_tweets
    def initialize(tweet,keywords=[])
      @search_keywords = Array(keywords)
      @tweet = tweet
      @score = 0
      @link_score = 0
      @percentile_score = 0
      @conversation_score = 0
      @follower_score = 0
      @retweet_score = 0
      @favorite_score =0
      @hashtag_score = 0
      @conversation_tweets = []
    end

    def is_conversation?
      self.tweet.in_reply_to_status_id?
    end

    def add_conversation_score
      self.conversation_score = CONVERSATION_SCORE if self.is_conversation?
    end

    def add_follower_score
      self.follower_score = FOLLOWER_SCORE*self.tweet.user.followers_count.to_f
    end

    def add_retweet_score
      self.retweet_score = RETWEET_SCORE*self.tweet.retweet_count.to_f
    end

    def add_favorite_score
      self.favorite_score = RETWEET_SCORE*self.tweet.favorite_count.to_f
    end


    def add_hashtag_score
      keywords = self.search_keywords.join(",")
      if keywords

        self.hashtag_score = 0
        self.tweet.hashtags.each do |hashtag|
          self.hashtag_score = self.hashtag_score+HASHTAG_SCORE if keywords.downcase.match(hashtag.text.downcase)
        end
      end
    end

    def add_scores
      add_conversation_score
      add_follower_score
      add_retweet_score
      add_favorite_score
      add_hashtag_score
    end

    def get_score
      add_scores
      set_total_score
      self.total_score
    end

    def set_total_score
      self.total_score = (self.link_score.to_f+self.conversation_score.to_f+self.follower_score.to_f+self.retweet_score.to_f+self.favorite_score.to_f+self.hashtag_score).to_f.round(2)
    end

    def get_conversation
      @conversation = C4C::Conversation.new(self.tweet)
      process_conversation_tweets
    end

    def process_conversation_tweets
      @conversation.tweets.each do |tweet|
        c4c_tweet = C4C::Tweet.new(tweet,Array(@search_keywords))
        c4c_tweet.get_score
        @conversation_tweets << c4c_tweet
        
      end
    end
  end
end
