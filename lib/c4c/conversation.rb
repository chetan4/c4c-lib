module C4C
  class Conversation
    attr_accessor :tweets, :tweet_response

    def initialize(tweet)
      @tweet = tweet
      @tweets = [@tweet]
    end

    def self.get_conversation_by_id(tweet_id)
      tweet = get_tweet_by_id(tweet_id)
      conversation = C4C::Conversation.new(tweet)
      conversation.get_conversation
      conversation
    end

    def get_conversation
      if @tweets.last.in_reply_to_status_id
        tweet = C4C::Conversation.get_tweet_by_id(@tweets.last.in_reply_to_status_id)
        @tweets << tweet
        get_conversation
      end
    rescue Exception => e
      return ['there was an error' + "#{e.message}"]
    end

    def self.get_tweet_by_id(tweet_id)
      tweet_status = Twitter::Tweet.new(:id=>tweet_id)
      twitter_client = get_client
      tweet = twitter_client.status(tweet_status)
    end

    # TODO Refactor use this via module
    def self.get_client
      twitter_client =Twitter::REST::Client.new do |config|
        config.consumer_key        = C4C.configuration.consumer_key
        config.consumer_secret     = C4C.configuration.consumer_secret
        config.access_token        = C4C.configuration.access_token
        config.access_token_secret = C4C.configuration.access_token_secret
      end
    end
  end
end
